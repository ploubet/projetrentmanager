package com.ensta.rentmanager.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.utils.IOUtils;

public class ClientService {

	// TODO: Bonus: implementer le design pattern singleton pour cette classe
    /** Constructeur priv� */  
    /*private ClientService()
    {
    	//this.clientDao.getInstance();
    }
     
    // Holder 
    private static class ClientServiceHolder
    {       
        // Instance unique non pr�initialis�e
        private final static ClientService instance = new ClientService();
    }
 
    // Point d'acc�s pour l'instance unique du singleton
    public static ClientService getInstance()
    {
        return ClientServiceHolder.instance;
    }*/
	// TODO: Bonus: implementer un mecanisme injectant les dao necessaire pour ce service
	private ClientDao clientDao;
	private Client client;
	
	public ClientService() {
		this.clientDao = new ClientDao();
		}
	
	public long create(Client client) throws ServiceException {
		// TODO: Creer un client
		//Contr�le si le client est majeur.
		if ( (Period.between(client.getNaissance(), LocalDate.now()).getYears()) <= 18 )
			throw new ServiceException("Le client doit �tre majeur !");
		if (client.getNom()== "" || client.getPrenom()== "") {
			throw new ServiceException("Le nom et le pr�nom doivent �tre renseign�s.");
		}
		
		try {
			clientDao.create(client);
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la cr�ation du client.");	
		}
		return 0;
	}//fin de la m�thode create

	public Client findById(long id) throws ServiceException {
		// TODO: Recuperer un client par son id
		Optional<Client> optionalClient = Optional.empty();
		try {
			optionalClient = clientDao.findById(id);
			optionalClient.get().toString();
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la consultation du client "+id+" .");	
		}
		return optionalClient.get();
	}

	public List<Client> findAll() throws ServiceException {
		// TODO: Recuperer tous les clients
		List<Client> listeClients = new ArrayList<Client>();
		try {
			listeClients = clientDao.findAll();
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la consultation des clients.");	
		}
		return listeClients;
	}
		
	public long delete(Client client) throws ServiceException {
		// Supprimer un client
		try {
			clientDao.delete(client);
		} catch (DaoException e) {
			throw new ServiceException("Une erreur a eu lieu lors de la suppression du client.");	
		}
		return 0;
	}//fin de la m�thode delete
}
