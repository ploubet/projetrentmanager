package com.ensta.rentmanager.service;

import java.util.List;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicule;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dao.VehicleDao;

public class VehicleService {

	// TODO: Bonus: implementer le design pattern singleton pour cette classe
    /** Constructeur priv� */  
    private VehicleService()
    {}
     
    /** Holder */
    private static class VehicleServiceHolder
    {       
        /** Instance unique non pr�initialis�e */
        private final static VehicleService instance = new VehicleService();
    }
 
    /** Point d'acc�s pour l'instance unique du singleton */
    public static VehicleService getInstance()
    {
        return VehicleServiceHolder.instance;
    }
	// TODO: Bonus: implementer un mecanisme injectant les daos necessaire pour ce service
		
		
	public long create(Vehicule vehicle) throws ServiceException {
		// TODO: Creer un vehicule
		return 0;
	}

	public Vehicule findById(long id) throws ServiceException {
		// TODO: Recuperer un vehicule par son id
		return null;
	}

	public List<Vehicule> findAll() throws ServiceException {
		// TODO: Recuperer tous les vehicules
		return null;
	}
	
}
