package com.ensta.rentmanager.ui.cli;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.utils.IOUtils;


public class monControleur {
	//Attribut
	private ClientService clientService;

	//Constructeur
	private monControleur() {
		this.clientService = new ClientService();
	}
	
	public static void main(String[] args) {
		monControleur cmd = new monControleur();
					
		String monCHoix;
		do {
			// Affichage du menu	
			System.out.println("Menu : ");
			System.out.println("1 - Cr�er un client");
			System.out.println("2 - Lister les clients");
			System.out.println("3 - Supprimer un client");
			System.out.println("4 - Cr�er un v�hicule -- EN COURS");
			System.out.println("5 - Lister tous les v�hicules-- EN COURS");
			System.out.println("6 - Supprimer un v�hicule-- EN COURS");
			System.out.println("q pour Quitter");	
			
			//Lecture du choix de l'utilisateur
			monCHoix = IOUtils.readString();
			switch (monCHoix){
			    case "1":
			    	System.out.println("------- Creation d'un client -------");
			    	System.out.println("Saisir le nom : ");
			    	String nomClient = IOUtils.readString();
			    	System.out.println("Saisir le pr�nom : ");
			    	String prenomClient = IOUtils.readString();
			    	System.out.println("Saisir l'email : ");
			    	String emailClient = IOUtils.readString();
			    	LocalDate dateCLient = IOUtils.readDate("Saisir la date de naissance : ", true);
			    	
			    	//Cr�ation et renseignement de l'objet Client
					Client monClient = new Client(nomClient,prenomClient,emailClient,dateCLient);
					//Test des infos du client
					System.out.println(monClient.toString());
					
					try{
						//Lancement du service de cr�ation
						cmd.clientService.create(monClient);
						System.out.println("Le client "+ monClient.getNom() + " a �t� cr��.");
					}
					catch (ServiceException e) {
							System.out.println("Une erreur a eu lieu lors de la cr�ation du client.");
					}			    
			    	break;
			    	
			    case "2":
			    	System.out.println("------- Liste des clients -------");
			    	try{
						//Lancement du service de r�cup�ration des clients
			    		List<Client> listeClients = cmd.clientService.findAll();
			    		//Affichage
			    		for (int i = 0; i < listeClients.size(); i++) {
			    			System.out.println(listeClients.get(i).toString());
			    		}
					}
					catch (ServiceException e) {
							System.out.println("Une erreur a eu lieu lors de l'affichage des clients.");
					}
			        break;
			        
			    case "3":
			    	System.out.println("------- Suppression d'un client -------");
			    	int idClient = IOUtils.readInt("Saisir l'id du client : ");
			    	try{
			    		//R�cup�ration du client recherch�
			    		Client testClient = cmd.clientService.findById((long)idClient);
			    		//testClient.toString();
						//Lancement de la recherche du client et confirmation de suppression
						System.out.println("Souhaitez-vous supprimer le client "+ testClient.getNom() + " de la base de donn�es ? (Y/N).");
						String confirmation = IOUtils.readString();
				    	if (confirmation.equals("Y")) {
					    	try{
								//Lancement du service de suppression
								cmd.clientService.delete(testClient);
								System.out.println("Le client "+ testClient.getNom() + " a �t� supprim�.");
							}
							catch (ServiceException e) {
									System.out.println("Une erreur a eu lieu lors de la suppression du client");
							}
				    	}
		
					}
					catch (ServiceException e) {
							System.out.println("Une erreur a eu lieu lors de la recherche du client");
					}
			    				        
			        break;
			    case "4":
	
			        break;
			    case "5":
			        break;
			    case "6":
			    	
			        break;
				} //fin du switch
		} while (!monCHoix.equals("q"));

	}//fin du main

}

