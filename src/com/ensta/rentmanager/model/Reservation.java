package com.ensta.rentmanager.model;

//Pour utiliser le type LocalDate
import java.time.LocalDate;

public class Reservation {
	
	private static int NOMBRE_DE_RESERVATIONS = 0;
	
	private int client_id;
	private int vehicle_id;
	private LocalDate debut;
	private LocalDate fin;
	
	//Constructeur	
	public Reservation(int client_id, int vehicle_id, LocalDate debut, LocalDate fin) {
	this.client_id = client_id;
	this.vehicle_id = vehicle_id;
	this.debut = debut;
	this.fin = fin;
	NOMBRE_DE_RESERVATIONS++;
	}
	
	//Getters
	public int getClient_id() { return client_id; }
	public int getVehicle_id() { return vehicle_id; }
	public LocalDate getDebut() { return debut; }
	public LocalDate getFin() { return fin; }
	
	//Setters
	public void setClient_id(int client_id) { this.client_id = client_id; }
	public void setVehicle_id(int vehicle_id) { this.vehicle_id = vehicle_id; }
	public void setDebut(LocalDate debut) { this.debut = debut; }
	public void setFin(LocalDate fin) { this.fin = fin; }
		
	public static int getCommunityNumber() {
		return NOMBRE_DE_RESERVATIONS;
		}
	
	public String toString() {
		return "Reservation{" +	"client_id=" + client_id +	", vehicle_id=" + vehicle_id 
				+	", debut=" + debut +	", fin =" + fin + '\'' +	'}';
		}
	
	}
