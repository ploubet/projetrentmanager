package com.ensta.rentmanager.model;

public class Vehicule {
	
private static int NOMBRE_DE_VEHICULES = 0;
	
	private String constructeur;
	private String modele;
	private int nb_places;
	
	//Constructeur	
	
	public Vehicule(String constructeur, String modele, int nb_places) {
	this.constructeur = constructeur;
	this.modele = modele;
	this.nb_places = nb_places;
	NOMBRE_DE_VEHICULES++;
	}
	
	//Getters
	public String getConstructeur() { return constructeur; }
	public String getModele() { return modele; }
	public int getNb_places() { return nb_places; }
	
	
	//Setters
	public void setConstructeur(String constructeur) { this.constructeur = constructeur; }
	public void setModele(String modele) { this.modele = modele; }
	public void setNb_places(int nb_places) { this.nb_places = nb_places; }
	
	public static int getCommunityNumber() {
		return NOMBRE_DE_VEHICULES;
		}
	
	public String toString() {
		return "Vehicule{" +	"constructeur=" + constructeur +	", modele=" + modele
				+	", nb_places=" + nb_places + '\'' +	'}';
		}
	
}
