package com.ensta.rentmanager.model;

//Pour utiliser le type LocalDate
import java.time.LocalDate;

public class Client {
	
	private int NOMBRE_DE_CLIENTS = 0;
	
	private Long id;
	private String nom;
	private String prenom;
	private String email;
	private LocalDate naissance;
	
	//Constructeurs
	public Client() {
	}
	
	public Client(Long id, String nom, String prenom, String email, LocalDate naissance) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.naissance = naissance;
		NOMBRE_DE_CLIENTS++;
		}
	
	public Client(String nom, String prenom, String email, LocalDate naissance) {
	this.nom = nom;
	this.prenom = prenom;
	this.email = email;
	this.naissance = naissance;
	NOMBRE_DE_CLIENTS++;
	}
	
	//Getters
	public String getNom() { return nom; }
	public String getPrenom() { return prenom; }
	public String getEmail() { return email; }
	public LocalDate getNaissance() { return naissance; }
	public Long getId() { return id; }
	
	//Setters
	public void setId(Long id) { this.id = id; }
	public void setNom(String nom) { this.nom = nom; }
	public void setPrenom(String prenom) { this.prenom = prenom; }
	public void setEmail(String email) { this.email = email; }
	public void setNaissance(LocalDate naissance) { this.naissance = naissance; }
	
	public int getCommunityNumber() {
		return NOMBRE_DE_CLIENTS;
		}
	
	public String toString() {
		return "Client{" +	"nom=" + nom +	", prenom=" + prenom 
				+	", email=" + email +	", naissance=" + naissance +	"}";
		}
		
	}
