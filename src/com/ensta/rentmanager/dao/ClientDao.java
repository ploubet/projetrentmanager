package com.ensta.rentmanager.dao;

import java.util.ArrayList;
import java.util.List;

//Classe Connection
import java.sql.Connection;
//Classe PreparedStatement et Resultest
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//Utilisation du type LocalDate et Date
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import java.util.Optional;

import java.sql.SQLException;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.persistence.ConnectionManager;
import com.ensta.rentmanager.utils.IOUtils;

public class ClientDao {
	
	// TODO: Bonus: implementer le design pattern singleton pour cette classe
    /** Constructeur priv� */  
    /*private ClientDao()
    {}
     
    // Holder
    private static class ClientDaoHolder
    {       
        // Instance unique non pr�initialis�e
        private final static ClientDao instance = new ClientDao();
    }
 
    // Point d'acc�s pour l'instance unique du singleton
    public static ClientDao getInstance()
    {
        return ClientDaoHolder.instance;
    }*/
	
	// TODO: Creer les chaines de caracteres qui representerons les requetes sql
	private static final String CREATE_CLIENT_QUERY = "INSERT INTO Client(nom, prenom, email, naissance) VALUES (?, ?, ?, ?);";
	private static final String FIND_CLIENT_QUERY = "SELECT id, nom, prenom, email, naissance FROM Client WHERE id = ?;";
	private static final String FIND_ALLCLIENT_QUERY = "SELECT id, nom, prenom, email, naissance FROM Client;";
	private static final String DELETE_CLIENT_QUERY = "DELETE FROM Client WHERE id = ?;";

	//CREATION d'un CLIENT
	public int create(Client client) throws DaoException {
		// TODO: Exercice 2 A completer
		
		//Initialisation de la variable renvoyant l'ID du client
		int id=0;
		try {
			//Création d'une connexion à la BDD
			Connection connection = ConnectionManager.getConnection();
			
			//Création d'un statement protégé contre les injections SQL
			PreparedStatement ps = connection.prepareStatement(CREATE_CLIENT_QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
			
			//Récupération des attributs du client pour la requête SQL
			ps.setString(1, client.getNom().toUpperCase());
			ps.setString(2, client.getPrenom());
			ps.setString(3, client.getEmail());
			ps.setObject(4, Date.valueOf(client.getNaissance()));

			//Execution de la requête
			ps.execute();
			
			//R�cup�ration de l'ID du Client
			ResultSet resultSet = ps.getGeneratedKeys();
			if (resultSet.next()) {
			    id = resultSet.getInt(1);
			}
			
			ps.close();
			connection.close();
		}
		catch (SQLException e) {
			throw new DaoException();
		}
		System.out.println(id);
		return id;
	}//fin de la m�thode create
	
	public long delete(Client client) throws DaoException {
		// TODO: Exercice 2 A completer
		try {
			//Création d'une connexion à la BDD
			Connection connection = ConnectionManager.getConnection();
			
			//Création d'un statement protégé contre les injections SQL
			PreparedStatement ps = connection.prepareStatement(DELETE_CLIENT_QUERY);
			
			//Récupération des attributs du client pour la requête SQL
			ps.setLong(1, client.getId());

			//Execution de la requête
			ps.execute();
			
			ps.close();
			connection.close();
		}
		catch (SQLException e) {
			throw new DaoException();
		}
		return 0;
	}//fin de la m�thode delete

	public Optional<Client> findById(long id) throws DaoException {
		// TODO: Exercice 2 A completer
		Client clientTrouve = new Client();
		try {
			//Création d'une connexion à la BDD
			Connection connection = ConnectionManager.getConnection();
			
			//Création d'unstatement protégé contre les injections SQL
			PreparedStatement ps = connection.prepareStatement(FIND_CLIENT_QUERY);
			
			//Récupération des attributs du client pour la requête SQL
			ps.setLong(1, id);
			
			//Execution de la requête
			ps.execute();
			while (ps.getResultSet().next()){	
				System.out.println(ps.getResultSet().getString(2) + " - " + ps.getResultSet().getString(3)+ " - " + ps.getResultSet().getString(4)+ " - " + ps.getResultSet().getString(5) );
				//Formattage de la date de naissance
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d HH:mm:ss");
				LocalDate dateNaissance = LocalDate.parse(ps.getResultSet().getString(5), formatter);
				System.out.println(dateNaissance);
				//Cr�ation et renseignement de l'objet Client
				clientTrouve.setId(id);
				clientTrouve.setNom(ps.getResultSet().getString(2));
				clientTrouve.setPrenom(ps.getResultSet().getString(3));
				clientTrouve.setEmail(ps.getResultSet().getString(4));
				clientTrouve.setNaissance(dateNaissance);
				System.out.println(clientTrouve.toString());
			}
			ps.close();
			connection.close();
		}
		catch (SQLException e) {
			throw new DaoException();
		}
		return Optional.ofNullable(clientTrouve);
	}//fin de la m�thode FindById

	public List<Client> findAll() throws DaoException {
		// Cr�ation d'une lsite de Client
		List<Client> listeClients = new ArrayList<Client>();

		try {
			//Création d'une connexion à la BDD
			Connection connection = ConnectionManager.getConnection();
			
			//Création d'un statement protégé contre les injections SQL
			PreparedStatement ps = connection.prepareStatement(FIND_ALLCLIENT_QUERY);

			//Execution de la requête
			ps.execute();						
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d HH:mm:ss");
			
			//Boucle sur les r�sultats et ajout � la liste
			while (ps.getResultSet().next()){				 
				//Formate la date de naissance
				LocalDate dateNaissance = LocalDate.parse(ps.getResultSet().getString(5), formatter);
				//Ajout � la liste de Clients
				listeClients.add(new Client(ps.getResultSet().getLong(1),ps.getResultSet().getString(2),ps.getResultSet().getString(3),ps.getResultSet().getString(4),dateNaissance));
			}    		
			
			ps.close();
			connection.close();
		}
		catch (SQLException e) {
			throw new DaoException();
		}	
		
		return listeClients;
	}//fin de la m�thode FindAll
	
	
}
