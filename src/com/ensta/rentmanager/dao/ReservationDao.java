package com.ensta.rentmanager.dao;

import java.util.List;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Reservation;

public class ReservationDao {

	// TODO: Bonus: implementer le design pattern singleton pour cette classe
    /** Constructeur priv� */  
    private ReservationDao()
    {}
     
    /** Holder */
    private static class ReservationDaoHolder
    {       
        /** Instance unique non pr�initialis�e */
        private final static ReservationDao instance = new ReservationDao();
    }
 
    /** Point d'acc�s pour l'instance unique du singleton */
    public static ReservationDao getInstance()
    {
        return ReservationDaoHolder.instance;
    }
	// TODO: Creer les chaines de caracteres qui representerons les requetes sql
	
	
	public long create(Reservation reservation) throws DaoException {
		// TODO: Exercice 2 A compl�ter
		return 0;
	}
	
	public long delete(Reservation reservation) throws DaoException {
		// TODO: Exercice 2 A compl�ter
		return 0;
	}

	
	public List<Reservation> findResaByClientId(long clientId) throws DaoException {
		// TODO: Exercice 2 A compl�ter
		return null;
	}
	
	public List<Reservation> findResaByVehicleId(long vehicleId) throws DaoException {
		// TODO: Exercice 2 A compl�ter
		return null;
	}

	public List<Reservation> findAll() throws DaoException {
		// TODO: Exercice 2 A compl�ter
		return null;
	}
}
