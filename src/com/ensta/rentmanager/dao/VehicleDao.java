package com.ensta.rentmanager.dao;

import java.util.List;
import java.util.Optional;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.model.Vehicule;

public class VehicleDao {
	
	// TODO: Bonus:  implementer le design pattern singleton pour cette classe
    /** Constructeur priv� */  
    private VehicleDao()
    {}
     
    /** Holder */
    private static class VehicleDaoHolder
    {       
        /** Instance unique non pr�initialis�e */
        private final static VehicleDao instance = new VehicleDao();
    }
 
    /** Point d'acc�s pour l'instance unique du singleton */
    public static VehicleDao getInstance()
    {
        return VehicleDaoHolder.instance;
    }
	// TODO: Creer les chaines de caracteres qui representerons les requetes sql
	
	public long create(Vehicule vehicle) throws DaoException {
		// TODO: Exercice 2 A completer
		return 0;
	}

	public long delete(Vehicule vehicle) throws DaoException {
		// TODO: Exercice 2 A completer
		return 0;
	}

	public Optional<Vehicule> findById(long id) throws DaoException {
		// TODO: Exercice 2 A completer
		return null;
	}

	public List<Vehicule> findAll() throws DaoException {
		// TODO: Exercice 2 A completer
		return null;
	}
	

}
